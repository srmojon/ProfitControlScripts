# ProfitControlScripts
Repositorio con los scripts necesarios para la aplicacion ProfitControl.

Scripts basados en el repositorio: https://bitbucket.org/cryptopopolis/profittrailer.git

# Install
Se clona el repositorio dentro de /opt
  cd /opt
  git clone https://gitlab.com/srmojon/ProfitControlScripts.git

#
# Scripts
#

# profit-install.py
Instala Profit Trailer en /opt/ProfitTrailer, requiere X parametros.

  Usage: python scripts/profit-install.py exchange apikey1 apisecret1 apikey2 apisecret2 license

Lista de parametros:
  exchange: Nombre del exchange (BITTREX, BINANCE o POLONIEX)

# profit-update.py
Actualiza la version del Profit Trailer
 Usage: python scripts/profit-update.py
