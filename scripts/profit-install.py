#!/usr/bin/env python
# modificacion del repositorio: https://bitbucket.org/cryptopopolis/profittrailer.git
# uso: python profittrailer.py exchange apikey1 apisecret1 apikey2 apisecret2

import os
import json
import urllib2
import zipfile
import platform
from shutil import copyfile
import re
import fileinput
import pwd
import sys

# guarda parametros
exch = sys.argv[1]
api1 = sys.argv[2]
sec1 = sys.argv[3]
api2 = sys.argv[4]
sec2 = sys.argv[5]
license = sys.argv[6]

# descarga ultima version en /tmp
def fileDownload():
    d = json.loads(urllib2.urlopen(
        "https://api.github.com/repos/taniman/profit-trailer/releases/latest").read())
    url = d["assets"][0]["browser_download_url"]
    file_name = d["assets"][0]["name"]
    dloc = "/tmp/%s" % file_name
    u = urllib2.urlopen(url)
    f = open(dloc, 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print("Downloading: %s Bytes: %s") % (file_name, file_size)

    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (
            file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8) * (len(status) + 1)
        print(status),

    f.close()
    procFile(dloc)

# extrae zip en /opt
def procFile(data):
    zip = zipfile.ZipFile(data)
    os.system("rm -rf /opt/ProfitTrailer")
    zip.extractall('/opt/')
    os.system("rm -rf /tmp/ProfitTrailer.zip")

# bypass con el exchange
def getExchange():
    global exch
    getApiKeys(exch)

# bypass con las apis y el exchange
def getApiKeys(exch):
    global license, api1, sec1, api2, sec2
    ptlic = license
    createConfig(exch, ptlic, api1, sec1, api2, sec2)

# modifica opciones de application.properties
def createConfig(exch, ptlic, api1, sec1, api2, sec2):
    #modifica archivo application
    for line in fileinput.input(["/opt/ProfitTrailer/application.properties"], inplace=1):
        line = re.sub('trading.exchange =', 'trading.exchange = %s' % exch.upper(), line.strip())
        line = re.sub('your license', '%s' % ptlic, line.strip())
        line = re.sub('engb', 'enus', line.strip())
        line = re.sub('default_api_key          =', 'default_api_key          = %s' % api1, line.strip())
        line = re.sub('default_api_secret       =', 'default_api_secret       = %s' % sec1, line.strip())
        line = re.sub('trading_api_key          =', 'trading_api_key          = %s' % api2, line.strip())
        line = re.sub('trading_api_secret       =', 'trading_api_secret       = %s' % sec2, line.strip())
        line = re.sub('server.timezone_offset = +00:00', 'server.timezone_offset = +02:00', line.strip())
        print(line)
    startUpScripts()

# crea usuario y servicio
def startUpScripts():
    oper = str(platform.dist()[0])
    def finduser(name):
        try:
            return pwd.getpwnam(name)
        except KeyError:
            return None

    if not finduser("profit"):
        print("creating user...")
        os.system("useradd profit -d /opt -s /bin/false -r")
    else:
        print("user already exists")

    os.system("chown -R profit:profit /opt/ProfitTrailer")

    if oper == 'centos' or oper == 'Ubuntu' or oper == 'redhat' or oper == 'debian':
        copyfile("/opt/ProfitControlScripts/files/profit.service",
                 "/etc/systemd/system/profit.service")
        os.system("cp -r /opt/ProfitControlScripts/files/profit.service.d /etc/systemd/system/")
    else:
        print("Sorry, your OS is not currently supported")
        exit(1)
    jdkInstall()

# instala JDK
def jdkInstall():
    oper = str(platform.dist()[0])
    if oper == 'Ubuntu' or oper == 'debian':
        os.system("apt-get update -y")
        os.system("apt-get install default-jdk -y")
    else:
        print("Not a supported OS")
    if oper == 'centos' or oper == 'redhat':
        os.system("yum install -y java-1.8.0-openjdk")
    else:
        print("Not a supported OS")
    finalSteps()

# recarga systemd y activa el servicio
def finalSteps():
    os.system("rm -f /opt/ProfitTrailer/initialization/*")
    os.system("cp /opt/ProfitControlScripts/files/ConfigFiles/* /opt/ProfitTrailer/initialization/")
    os.system("mkdir /opt/ProfitTrailer/trading")
    os.system("cp /opt/ProfitControlScripts/files/ConfigFiles/* /opt/ProfitTrailer/trading/")
    os.system("systemctl daemon-reload")
    #os.system("systemctl enable profit")
    #os.system("systemctl start profit")

if __name__ == '__main__':
    fileDownload()
    getExchange()
